import Vue from 'vue'
import App from './App.vue'
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import './css/mdi.css';
import 'vuetify/dist/vuetify.min.css'
import Vuetify from 'vuetify/lib/framework'
import ajaxpost from "./lib/ajaxpost.js";
import ru from 'vuetify/es5/locale/ru';
import store from './store';
import VueCropper from 'vue-cropperjs';
import 'cropperjs/dist/cropper.css';
Vue.component('VueCropper',VueCropper);

Vue.config.productionTip = false;

Vue.use(Vuetify);
Vue.use(ajaxpost);

const vuetify = new Vuetify({
    lang: {
        locales: { ru },
        current: 'ru',
    },
    icons: {
        iconfont: "mdi"
    },
});

new Vue({
    vuetify,
    store,
    render: h => h(App)
}).$mount('#app');


