import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export default new Vuex.Store({
    state: {
        auth: {},
        api: {
            urlAuth: 'https://sudapi.rivg.ru',
            // urlAuth: 'http://regapi',
        },
        apiFull: {
            wialon: 'http://wialonapi.stak63.com',
            // wialon: 'http://wialonapi',
        },
        confirm: {
            head: '',
            text: '',
            action: () => {}
        },
        notify: {
            text: '',
            color: ''
        },
        loading:false,
    },
    getters:{
        get: state => state,
        user: state => state.auth.info,
    },
    mutations: {
        set:(state,payload)=>state[payload.prop] = payload.value,
        confirm(state, payload) {
            state.confirm = {
                head: (('head' in payload) && !!payload.head) ? payload.head : "Подтвердите действие",
                text: payload.text,
                action: payload.action,
            };
        },
        notify(state, payload) {
            state.notify = {
                text: (typeof payload == 'object') ? payload.text : payload,
                color: (typeof payload == 'object') ? payload.color : 'error'
            };
        },
    },
    actions: {
        set:({commit}, payload)=>commit("set", payload),
        confirm({commit}, payload) {
            commit("confirm", payload);
        },
        notify({commit}, payload) {
            commit("notify", payload);
        },
    },
});