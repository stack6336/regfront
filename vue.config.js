module.exports = {
  "runtimeCompiler": true,
  "transpileDependencies": [
    "vuetify"
  ],
  publicPath:"./",
  productionSourceMap: false
};